import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;



public class Plugin_Frame implements PlugInFilter {
protected ImagePlus imp;
public int setup(String arg, ImagePlus imp) {
this.imp = imp;
return DOES_8G;
}
public void run(ImageProcessor ip) {
// ip.invert(); compiled bnuild in method
int M = ip.getWidth();
int N = ip.getHeight();

//iterate  over all images coordinates (u,v)
for (int u = 0; u < M; u++){
    for(int v = 0; v < N; v++){ 
        int p = ip.getPixel(u, v);
        ip.putPixel(u, v, 255 - p);
    }
}



}
}