package Template;

import ij.plugin.filter.PlugInFilter;
import ij.ImagePlus;
import ij.process.ImageProcessor;

/**
 *
 * @author sergio
 */
public class Compute_Histogram implements PlugInFilter {
    
    public int setup(String arg, ImagePlus img){
        return DOES_8G + NO_CHANGES;
    }
    
    public void run(ImageProcessor ip){
        int[] h = new int[256]; // histogram array
        int W = ip.getWidth();
        int H = ip.getHeight();
        
        for (int v = 0; v < H; v++ ){
            for ( int u = 0; u < W; u++){
                int i = ip.getPixel(u, v );
                h[i] = h[i]+1; // counting the frequency for each position
            }
        }
    }
    
}
